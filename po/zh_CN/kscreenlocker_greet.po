msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-22 00:46+0000\n"
"PO-Revision-Date: 2022-10-02 15:49\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/kscreenlocker/kscreenlocker_greet.pot\n"
"X-Crowdin-File-ID: 4447\n"

#: main.cpp:116
#, kde-format
msgid "Greeter for the KDE Plasma Workspaces Screen locker"
msgstr "KDE Plasma 工作空间屏幕锁定程序"

#: main.cpp:120
#, kde-format
msgid "Starts the greeter in testing mode"
msgstr "以测试模式启动欢迎画面"

#: main.cpp:123
#, kde-format
msgid "Starts the greeter with the selected theme (only in Testing mode)"
msgstr "用指定主题启动欢迎画面 (仅在测试模式中)"

#: main.cpp:127
#, kde-format
msgid "Lock immediately, ignoring any grace time etc."
msgstr "立即锁定，忽略任何等待时间。"

#: main.cpp:129
#, kde-format
msgid "Delay till the lock user interface gets shown in milliseconds."
msgstr "显示锁屏界面前的延迟毫秒数。"

#: main.cpp:132
#, kde-format
msgid "Don't show any lock user interface."
msgstr "不显示锁屏界面。"

#: main.cpp:133
#, kde-format
msgid "Default to the switch user UI."
msgstr "默认为切换用户界面。"

#: main.cpp:135
#, kde-format
msgid "File descriptor for connecting to ksld."
msgstr "连接到 ksld 的文件描述符。"
